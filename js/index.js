document.querySelectorAll('p').forEach(function(elem){
    elem.style.background = '#ff0000'
});

let optionsList = document.getElementById("optionsList");
console.log('element by ID "optionsList": ', optionsList);

let parentNode = optionsList.parentNode;
console.log('parentNode: ', parentNode);

let childrenNodes = optionsList.children;
console.log('childrenNodes: ', childrenNodes);

let testParagraph = document.getElementById("testParagraph");
testParagraph.textContent = "This is a paragraph";
console.log('testParagraph: ', testParagraph);

let mainHeader = document.querySelector(".main-header").children;
console.log(mainHeader);
for (let elem of mainHeader) {
	console.log(elem.classList.add(".nav-item"));
}
let sectionTitle = document.querySelectorAll(".section-title");
console.log(sectionTitle);
for(let elem of sectionTitle){
    console.log(elem);
    elem.classList.remove("section-title");
}
console.log(document.querySelectorAll(".section-title"));